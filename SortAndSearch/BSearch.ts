/// for testing search
import {BSort} from './BSort';

const target = 9;
const sortedArray = BSort.sortB;
let count = 0;
export class BSearch {
  static get bsearch() {
  return bSearch(sortedArray, target);
  }
}


function bSearch(sortedArray, target) {
	let start = 0;
	let end = sortedArray.length - 1;
	while (start <= end) {
		const mid = start + Math.floor((end - start) / 2);
		count ++;
		if (sortedArray[mid] === target){
			return mid;
			
		}
		if (sortedArray[mid] < target) {
			start = mid + 1;
		} else {
			end = mid -1;
		}
	}
	return -1;
}

console.log(BSearch.bsearch);
console.log(count);
